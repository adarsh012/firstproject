import dataclasses
from telnetlib import STATUS
from firstapp.serializers import EmployeeSerializer
from rest_framework import status
from django.shortcuts import render
from firstapp.models import employee
from rest_framework.response import Response
from rest_framework.views import APIView


class empdetail(APIView):
    def post(self, request):

        try:
            data = request.data
            user = employee()
            user.fullname = data["fullname"]
            user.emp_code = data["emp_code"]
            user.mobile = data["mobile"]
            user.save()
            return Response(ResponseReturn.successfn({"Detail": "Record Added Successfully"}), status=status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status=STATUS.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        try:
            employeedetail = employee.objects.all()
            serializer = EmployeeSerializer(employeedetail, many=True)
            return Response(ResponseReturn.successfn(serializer.data), status=status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"status": "Not Found" + str(e)}), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        try:
            data = request.data["id"]
            cvVersionDataModel = None
        
            cvVersionDataModel = employee.objects.get(id=data)
            cvVersionDataModel.delete()
            return Response(ResponseReturn.successfn({"detail": "Record erased successfully if existed"}))
        except:
                return Response(ResponseReturn.failurefn({"status": "Record Not Found"}))
    def put(self, request):
        
        try:
            data = request.data
            user = employee.objects.get(id=data["id"])
            user.fullname = data["fullname"]
            user.emp_code = data["emp_code"]
            user.mobile = data["mobile"]
            user.save()
            return Response(ResponseReturn.successfn({"Detail": "Record Updated Successfully"}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)



class ResponseReturn:
    def successfn(data):
        res = {}
        res["type"] = "Success"
        res["data"] = data
        return res

    def failurefn(data):
        res = {}
        res["type"] = "Fail"
        res["data"] = data
        return res
